<?php
    require_once ('binatang.php');
    require ('ape.php');
    require ('frog.php');
    $sheep = new Binatang("shaun");
    echo "name = " . $sheep->name . "<br>";
    echo "leg = " . $sheep->legs . "<br>";
    echo "cold blooded = " . $sheep->coldblooded . "<br><br>";

    $frog = new Binatang("buduk");
    echo "name = " . $frog->name . "<br>";
    echo "leg = " . $frog->legs . "<br>";
    echo "cold blooded = " . $frog->coldblooded . "<br>";
    echo "Jump = " . $frog->jump . "<br><br>";
    
  
    $ape = new Ape("sungokong");
    echo "name = " . $ape->name . "<br>";
    echo "leg = " . $ape->legs . "<br>";
    echo "cold blooded = " . $ape->coldblooded . "<br>";
    echo "yell = " . $ape->yell . "<br>";
    
?>